Param(
    [string]$TenantID = "6660b467-add3-4d9e-8ada-48b1721959f3",
    [string]$ServiceFabricClusterName = "kbservfabcluster",

    #URL of the Load Balancer internal IP
    [string]$WebAppReplyURL = "https://10.10.10.250:19080/Explorer/index.html"
)

# Path to AD service principal creation script
.\AAD_Helpers\SetupApplications.ps1 `
-TenantId $TenantID -ClusterName $ServiceFabricClusterName -WebApplicationReplyUrl $WebAppReplyURL
